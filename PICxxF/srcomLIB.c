#include <stdlibm.h>
#include <string.h>
#define SEP '\n'
#define END '\0'

struct SERIAL_LIB {
    char* sSerial;
}; typedef struct SERIAL_LIB SERIALsr;

SERIALsr serialData;
char str[9] = "\0";

// SEND SYSTEM
//Any of this functions, send data char by char
void set_SERIALsr() { //INIT Declaration
    serialData.sSerial = malloc(sizeof(char));
}
void sendFT(char t , float v, char a) { //t: Temp, Humity, etc || v: Float value || a: SEP or END
    sprintf(str, "%c%f%c", t, v, a);
    for(int i = 0; str[i] != a; i++)
        printf("%c", str[i]);
    printf("%c", a);
}
void sendINT(char t , int v, char a) { //t: Temp, Humity, etc || v: Int value || a: SEP or END
    sprintf(str, "%c%d%c", t, v, a);
    for(int i = 0; str[i] != a; i++)
        printf("%c", str[i]);
    printf("%c", a);
}
void sendSTR(char t , char v[], char a) { //t: Temp, Humity, etc || v: String value || a: SEP or END
    sprintf(str, "%c%s%c", t, v, a);
    for(int i = 0; str[i] != a; i++)
        printf("%c", str[i]);
    printf("%c", a);
}
void sendDATA(char s_[], char a) { //v: Int value || a: SEP or END
    for(int i = 0; s_[i] != a; i++)
        printf("%c", s_[i]);
    printf("%c", a);
}
// END SEND SYSTEM
// READ SERIAL
int getArrSize() { return strlen(serialData.sSerial); } //Get Size of array when reading COM Serial
int insert(char c_) { //Insert read value into a dynamic array
    int i = 0;
    serialData.sSerial = realloc(serialData.sSerial, sizeof(char)*(i + 1));
    if(serialData.sSerial != NULL) {
        i++;
        *(serialData.sSerial + getArrSize() - 1) = c_;
        return TRUE; //Data was able to save
    } return FALSE; //No RAM able to increase array size
}
char* getSTR() { return serialData.sSerial; } //Return String from COM reading
// END READ SERIAL
